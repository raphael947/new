<?php

namespace Chatter\Middleware;
class Logging {
    public function __invoke($request,$response,$next){
    //befor route
    error_log($request->getMethod()."--".$request->getUri()."\r\n",3,"log.txt");
    $response = $next($request,$response);//תעבור למידל וור הבא, במקרה שלנו אין מידל וור הבא אז הוא הולך לראוט
    
    //after route
    return $response;
    }
}