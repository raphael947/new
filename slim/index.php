<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app ->add(new logging());//הוספה של טבעת מסביב  לראווט. כל פעם שאנחנו עושים עוד שאנחנןו מוסיפים טבעת נוספת 

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('your customer number is '.$args['number']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('customer '.$args['cnumber'].' purchased product number '.$args['pnumber'] );
});

//-------------------------------MESSAGES---------------------------------------------------------------------------------------
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();

    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            "body" => $msg->body,
            "user_id" => $msg->user_id,
            "created_at" => $msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); // משיכת השדה לגוף ההודעה
    $userid = $request->getParsedBodyParam('userid',''); // משיכת השדה לגוף ההודעה
    $_message = new Message();
    $_message->body =  $message; // גוף ההודעה שווה למה שהגיע בפוסט
    $_message->user_id =$userid;
    $_message->save();
    if($_message->id){ // אם יש להודעה איידי זה אומר שההודעה נשמרה כמו שצריך בדיבי
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }

});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message = Message::find($args['message_id']); // מציאת ההודעה הרלוונטית לפי האיידי
    $_message->delete();
    if($_message->exist){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
        
    }
});

//lesson 4 19/11/17
$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); // משיכת השדה לגוף ההודעה שהגיע מהפוסט
    $_message = Message::find($args['message_id']);//מחפש את ההודעה עם האיי די שהגיע מהיו אר אל
    //die("message id = ". $_message->id);//אפשרות לבדור את עצמי. הקוד יעבוד ע כאן
    $_message ->body =$message;// השורה שבה קורה העדכון של הכנסת ערך חדש להודעה
    if( $_message->save()){ // אם יש להודעה איידי זה אומר שההודעה נשמרה כמו שצריך בדיבי
        $payload = ['message_id'=>$_message->id,"result"=>"The message was updated successfuly"];//החזרה של קובץ גייסון כדי שהיוזר ידע אם התבצע חיבור (200 או 201 או 400)
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }

});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload =$request->getParsedBody();//ה"פארס" הופך את מה שקיבלנו כגייסון  למערך
    Message:: insert($payload);
    return $response->withStatus(201)->withJson($payload);
    }

);




//------------------------USERS---------------------------------------------------------------------------------
$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=> $usr->id,
            'username'=> $usr->username,
            'email'=> $usr->email];
    }
   return $response->withStatus(200)->withJson($payload);
});

//------------------------------------------------------------------------------------------------------------------
$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
   $_user = new User();
   $_user->username = $username;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});

//----------------------------------------------------------------------------------------------------------------------
$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']); 
    $user->delete();
    if($user->exist){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
        
    }
});
//-----------------------------------------------------------------------------------------------------------------------
$app->post('/users/bulk', function($request, $response,$args){
    $payload =$request->getParsedBody();//ה"פארס" הופך את מה שקיבלנו כגייסון  למערך
    User:: insert($payload);
    return $response->withStatus(201)->withJson($payload);
    }

);
//-------------------------------------------------------------------------------------------------------------------------
//$app->delete('/users/bulk', function($request, $response,$args){
  //  $payload =$request->getParsedBody();//ה"פארס" הופך את מה שקיבלנו כגייסון  למערך
//User::trash($payload);
  //  return $response->withStatus(201)->withJson($payload);
  //  }


//);
//-----------------------------------3/12/17------------------------------------------------------
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);//תוסיף להאדר את האישור לדומיינים אחרים גם
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});


$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

/*$app->post('/auth', function($request, $response,$args){//צריך לקבל שם משתשמש וסיסמא, לבדוק זכאות, לשלוח לקליינט   גייסון וב טוקן מתאים 
$user=$request->getParsedBodyParam('user','');//קיבלנו את היוזרניים מהקליינט
$password=$request->getParsedBodyParam('password','');
//we need to do the DB but not now
if($user == 'jack' && $password =='1234'){//בדיקת זכאות
    //create jwt and send to client, we need to generate jwt but not now
    $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.PETyPBssJTN1V8fSwPxkE0Hftg3hGYux4Rx1WcMC5NA'];
    return $response->withStatus(200)->withJson($payload);
}else{
     $payload = ['token'=>null];//אם אין לו טוקן
    return $response->withStatus(403)->withJson($payload);
     }

   
});

$app->add(new \Slim\Middleware\JwtAuthentication([//הגנה על כל הראוטים- רק מי שיש לו גייסון ובב טוקן יקבל את הראוטים השונים
    "secret" => "supersecret",//מידל וור שבודק שכל גייסון טוקן שמגיע- האם הוא מאומתאו יותר נכון אמיתי
    "path" =>['/messages']//אלה הראוטרים שאנחנו רוצים שגיי דאבליו טי יפקח עליהם
]));*/
$app->run();